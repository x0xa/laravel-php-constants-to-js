<?php

namespace Hicks\LaravelPhpConstantsToJs\Exceptions;

use Exception;

class PhpConstantsToJsException extends Exception {}