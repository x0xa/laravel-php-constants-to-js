<?php
declare(strict_types = 1);

namespace Hicks\LaravelPhpConstantsToJs\Generators;

/**
 * Interface GeneratorContract
 *
 * @package Crazybooot\ConstantsToJs\Generators
 */
interface IGenerator
{
    /**
     * @param string $data
     *
     * @return string
     */
    public function generate(string $data): string;
}