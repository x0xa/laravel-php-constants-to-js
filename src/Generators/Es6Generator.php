<?php
declare(strict_types=1);

namespace Hicks\LaravelPhpConstantsToJs\Generators;

/**
 * Class Es6Generator
 *
 * @package Hicks\LaravelPhpConstantsToJs\Generators
 */
class Es6Generator implements IGenerator {
    /**
     * @param string $data
     *
     * @return string
     */
    public function generate(string $data): string {
        return "export default {$data}";
    }
}