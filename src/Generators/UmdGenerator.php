<?php
declare(strict_types=1);

namespace Hicks\LaravelPhpConstantsToJs\Generators;

/**
 * Class UmdGenerator
 *
 * @package Hicks\LaravelPhpConstantsToJs\Generators
 */
class UmdGenerator implements IGenerator {
    /**
     * @param string $data
     *
     * @return string
     */
    public function generate(string $data): string {
        return "(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
        typeof define === 'function' && define.amd ? define(factory) :
            (global.constants = factory());
}(this, (function () { 'use strict';
    return {$data}
})));";
    }
}