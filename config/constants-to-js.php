<?php
declare(strict_types = 1);

use Hicks\LaravelConstantsToJs\Generators\UmdGenerator;

return [
    /**
     * Resulting JavaScript object
     */
    'constants'     => [],

    /**
     * Path to generated JavaScript file
     */
    'target_path'   => public_path('js/constants.js'),

    /**
     * Format of generated JavasCript file Es6Generator or UmdGenerator
     */
    'generator' => UmdGenerator::class,
];